MAIN_UTILS = \
	after \
	beep \
	base64 \
	buildrun \
	cvt-utf8 \
	lns \
	multi \
	nntpid \
	pid \
	reservoir \
	xclipglue \
	xcopy \
# end of list

SUBDIRS = $(patsubst %/Makefile,%,$(wildcard */Makefile))
SUBDIRS_X11 = $(patsubst %/.need_x11,%,$(wildcard */.need_x11))
SUBDIRS_NOX = $(filter-out $(SUBDIRS_X11),$(SUBDIRS))

# for `make html' and `make release'; should be a relative path
DESTDIR = .

# for `make install'; should be absolute paths
PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
SCRIPTDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/man/man1
INSTALL = install
IPROG =#   flags for installing programs (default none)
IDATA = -m 0644  # flags for installing data

GITROOT = https://git.tartarus.org/simon

all: $(foreach subdir,$(SUBDIRS),$(subdir)/make)
all-nox: $(foreach subdir,$(SUBDIRS_NOX),$(subdir)/make)
progs: $(foreach subdir,$(SUBDIRS),$(subdir)/progs)
progs-nox: $(foreach subdir,$(SUBDIRS_NOX),$(subdir)/progs)
man: $(foreach subdir,$(SUBDIRS),$(subdir)/man)
clean: $(foreach subdir,$(SUBDIRS),$(subdir)/clean)
html: $(foreach subdir,$(SUBDIRS),$(subdir)/html)
release: $(foreach subdir,$(SUBDIRS),$(subdir)/release)
install: $(foreach subdir,$(SUBDIRS),$(subdir)/install)
install-nox: $(foreach subdir,$(SUBDIRS_NOX),$(subdir)/install)
install-progs: $(foreach subdir,$(SUBDIRS),$(subdir)/install-progs)
install-progs-nox: $(foreach subdir,$(SUBDIRS_NOX),$(subdir)/install-progs)
install-man: $(foreach subdir,$(SUBDIRS),$(subdir)/install-man)
install-man-nox: $(foreach subdir,$(SUBDIRS_NOX),$(subdir)/install-man)
git-pull: $(foreach subdir,$(SUBDIRS),$(subdir)/git-pull)

checkout: $(MAIN_UTILS)
	true
$(MAIN_UTILS): %:
	git clone $(GITROOT)/$@.git

SUBTARGETS = make clean progs man html install install-progs install-man release
PER_UTIL_SUBTARGETS = $(foreach st,$(SUBTARGETS),$(foreach subdir,$(SUBDIRS),$(subdir)/$(st)))
# The + prefix permits the sub-makes to share the job server if you
# run one of these targets using make -j.
$(PER_UTIL_SUBTARGETS): %:
	+make -C $(dir $@) $(patsubst make,,$(notdir $@)) $(EXTRA_MAKE_FLAGS)

$(foreach subdir,$(SUBDIRS),$(subdir)/release $(subdir)/html) : override EXTRA_MAKE_FLAGS=DESTDIR=../$(DESTDIR)

$(foreach subdir,$(SUBDIRS),$(subdir)/git-pull): %/git-pull:
	cd $(patsubst %/git-pull,%,$@) && git pull --ff-only
